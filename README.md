# LibreEquipment MGSE 2U TestPOD

The 2U TestPOD is used for vibration testing of 2U CubeSats.

![](docs/pictures/main.jpg)

## Getting Started

Find the design source files in folder `/src`. All information and files needed
for manufacturing are in folder `/build`.
